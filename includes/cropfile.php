<?php
/**
 * @project     NodevoCropHTML5
 * @date        12.10.28
 * @author      D.CAMUS - Nodevo <dcamus@nodevo.com>
 * @link        www.nodevo.com
 * 
 * PHP File croper (server side)
 * 
 * @todo        PNG Compliance
 */


if ($_SERVER['REQUEST_METHOD'] == 'POST')
{
	$targ_h = $_POST['nh'];
    $targ_w = $_POST['nw'];
	$jpeg_quality = $_POST['q'];

	$src = '../userfiles/'.$_POST['src'];
	$img_r = imagecreatefromjpeg($src);
	$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

	imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'],
	$targ_w,$targ_h,$_POST['w'],$_POST['h']);

	header('Content-type: image/jpeg');
    header('Content-Type: application/force-download');
    header('Content-Disposition: attachment; filename="'.$_POST['src'].'"');
	imagejpeg($dst_r,null,$jpeg_quality);

	exit;
}
?>
