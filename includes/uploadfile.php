<?php
/**
 * @project     NodevoCropHTML5
 * @date        12.10.28
 * @author      D.CAMUS - Nodevo <dcamus@nodevo.com>
 * @link        www.nodevo.com
 * 
 * PHP File uploader (server side)
 * 
 * @todo        Catch errors
 */


$picture = $_FILES["file"];
$newfilename = "nodevo_".md5(microtime())."_".$picture["name"];
move_uploaded_file($picture["tmp_name"], realpath('../userfiles/') . "/". $newfilename);

die($newfilename);

?>
