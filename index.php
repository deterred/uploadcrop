<?php
/**
 * @project     NodevoCropHTML5
 * @date        12.10.28
 * @author      D.CAMUS - Nodevo <dcamus@nodevo.com>
 * @link        www.nodevo.com
 * 
 * Main php File
 * 
 * @todo        Clean HTML Code
 */
?>
<html lang="en">
    <head>
        <meta charset=utf-8>
    </head>
    <link rel="stylesheet" href="css/jquery.Jcrop.css" type="text/css" />
    <!--<link rel="stylesheet" href="css/jquery-ui-1.9.1.custom.min.css" type="text/css" />-->
	<link rel="stylesheet" href="css/styles.css" type="text/css" />
    <script src="js/jquery.min.js"></script>
    <!--<script src="js/jquery-ui-1.9.1.custom.min.js"></script>-->
    <script src="js/jquery.Jcrop.min.js"></script>
    <script src="js/nodevo.dragandcrop.js"></script>
    
    <body>
    <div>
        <img src="img/logo_nodevo.jpg" style="margin-left:10px"/>
    </div>
    <div id="col1">
        <form method="post" action="/includes/uploadme.php" enctype="multipart/form-data" id="formFile">
            <div class="file_button_container">
            <input name='uploads[]' type="file" onchange="onFilesSelected(this.files);"/>
            </div>
        </form>
        <div id="dropZone" class="dropZone"><p>Or drop your file here</p></div>
    </div>
    <div id="col2">
        <h2>Options</h2>
        <div id="html5options">
            <br/><br/>
            Height (px): <br/>
            <input type="range" min="0" max="0" step="1" value ="0" style="width: 200px;" onchange="onUpdateRange(this.value,'h')" id="rheight" name="rheight"/><br/>
            <div class="legend"><i><span id="th"></span> px</i></div>
            
            
            Width (px): <br/>
            <input type="range" min="0" max="0" step="1" value ="0" style="width: 200px;" onchange="onUpdateRange(this.value,'w')" id="rwidth" name="rwidth"/><br/>
            <div class="legend"><i><span id="tw"></span> px</i></div>
            
            
            <input type="checkbox" checked="true" disabled/><small><i>Keep aspect ratio </i></small>
            
            <br/><br/><br/>
            Jpeg Quality (%): <br/>
            <input type="range" min="10" max="100" step="1" value ="90" style="width: 200px;" onchange="onUpdateRange(this.value,'q')" id="rqual" name="rqual"/><br/>
            <div class="legend"><i><span id="tq">90</span> %</i></div>

            <br/><br/>
        </div>
        <div id="nohtml5options">
            <div style="font-size:14px; text-align:center;font-weight:bold; width:180px; padding:10px; margin:20px auto; background:#666; color:white">
            Your browser does'nt support HTML5 components, please update !
            </div>            
            Height (px): <i><span id="th"></span> px</i>
            <br/><br/>   
            Width (px): <i><span id="tw"></span> px</i>
            <br/><br/>
            <br/><br/><br/>
            Jpeg Quality (%): <i><span id="tq">90</span> %</i>
            <br/><br/>
        </div>
        Preview :
        <div class="preview">
         <img src="" id="preview" alt="Preview" class="jcrop-preview" style="display:none"/>
         <img src="img/ajax-loader.gif" id="loader1" style="display:none"/>
        </div>
        <!-- Crop Form -->
	    <form action="includes/cropfile.php" method="post" onsubmit="return checkCoords();" target="_blank">
		    <input type="hidden" id="x" name="x" />
		    <input type="hidden" id="y" name="y" />
		    <input type="hidden" id="w" name="w" />
		    <input type="hidden" id="h" name="h" />
		    <input type="hidden" id="nw" name="nw" />
		    <input type="hidden" id="nh" name="nh" />
            <input type="hidden" id="q" name="q" value="90"/>
            <input type="hidden" id="src" name="src" />
		    <input type="submit" value="Get It !" style="width:200px" id="submitcrop" name="submitcrop"/>
            <img src="img/ajax-loader.gif" id="loader2" style="display:none"/>
	    </form>
    </div>
    <div id="clr"></div>

    <!-- AddThis Button BEGIN -->
    <div class="addthis_toolbox addthis_default_style ">
    <a class="addthis_button_facebook_like" fb:like:layout="button_count" style="margin-left:250px"></a>
    <a class="addthis_button_tweet"></a>
    <a class="addthis_button_google_plusone" g:plusone:size="medium" ></a> 
    </div>
    <script type="text/javascript" src="http://s7.addthis.com/js/300/addthis_widget.js#pubid=xa-508cf9464192c894"></script>
    <!-- AddThis Button END -->
    </body>
</html>
